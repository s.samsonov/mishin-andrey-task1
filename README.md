#  Данный плейбук предполагает создание пользователей на системе Red Hat


### Список устанавливаемого и задействованного ПО.
RHEL 7.6, Ansible 2.9.6, Python 2.7.5, 

### Требования:
1. базовый Linux 7.6.
2. наличие подключенных репозиториев RHEL ко всем серверам (base, extras, updates, tools, optional, rhscl).
3. доступ в интернет к вендорским сайтам для загрузки пакетов.
4. настроеный доступ по ключам для root пользователей со стороны Ansible-master.
5. пакет `python-netaddr` на Ansible-master
6. установка коллекций на Ansible-master (для версий 2.9+) командой: ```ansible-galaxy collection install community.docker``` и ```ansible-galaxy collection install community.general```
### Установка:
1. Установить ansible требуемой версии на одном из серверов имеющих непосредственный доступ по 22 порту: ```yum install ansible```
2. Получить код плейбуков из репозитория: ```git clone `https://gitlab.com/s.samsonov/mishin-andrey-task1.git``
3. Сгенерировать пару (публичный и приватный) ключей на ansible-master сервере для root пользователя (при наличии, пропустить этот шаг): ```ssh-keygen -t rsa```
4. Скопировать публичный ключ root пользователя на Ansible-master на все таргеты (root->root): ```ssh-copy-id -i ~/.ssh/id_rsa.pub root@xxx.xxx.xxx.xxx```
5. Задать переменные в ./role/default/main.yml, ./role/var/main.yml и ./group_vars/all
6. Выполнить развертывание системы посредством запуска плейбука со следующими опциями:
Запуск плейбука осуществляется на Ansible-master сервере следующим образом:

```
ansible-playbook -v -i hosts master.yml --extra-vars "env_state=present" -t <тег>

```

### Описание ролей:

1. **kafka**   Создание пользователя. Тег   `-t kafka`
2. **zookeeper**  Создание пользователя. Тег  `-t zoo`
3. **hadoop**   Создание пользователя. Тег   `-t hadoop`
4. **spark**   Создание пользователя. Тег   `-t spark`
5. **wso2ei**  Создание пользователя. Тег   `-t wso2ei`
6. **kubernetes**  Создание пользователя. Тег  `-t kubernetes`


